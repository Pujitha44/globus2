import React from 'react'
import Home from './components/Home';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Visa1 from './components/Visa1';
import Homes from './components/Homes';
// import { Carousel } from 'react-bootstrap';
import Carousel from './components/Carousel'
import Clip__path from './components/Clip__path';
import JoinNow from './components/JoinNow';
import SignUp from './components/SignUp';
import College from './components/College';
import { Product2, Product3, Product4 } from './components/Product2';


export default function App() {
  return (
    <Router>
      <div className="app">
        <Switch>
        <Route path="/visa">
            <Visa1/>
          </Route>

          <Route path="/signUp">
           <Home/>
            {/* <Visa1/> */}
            <SignUp/>
          </Route>
          
          <Route path="/joinnow">
            <Home/>
            <JoinNow/>
          </Route>

          <Route path="/">
            <Home/>
            <Carousel/>
            <Clip__path/>
            <College/>
            <div className="home__row">
                    
                    <Product2/>
                    <Product3/>
                    <Product4/>
                    <Product2/>
                </div>
            {/* <JoinNow/> */}
            {/* <Homes/> */}
          </Route>
        </Switch>
        
      </div>
    </Router>
    
    
  );
}


